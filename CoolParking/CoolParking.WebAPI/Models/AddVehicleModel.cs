﻿using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class AddVehicleModel
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}
