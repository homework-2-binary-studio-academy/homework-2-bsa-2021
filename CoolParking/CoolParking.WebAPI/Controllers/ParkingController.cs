﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public ParkingController(IParkingService parking)
        {
            parkingService = parking;
        }

        [HttpGet("balance")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<int> GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }

        [HttpGet("freePlaces")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}
