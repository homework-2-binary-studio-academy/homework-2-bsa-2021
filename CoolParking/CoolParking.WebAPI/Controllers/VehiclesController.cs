﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parking)
        {
            parkingService = parking;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<ICollection<Vehicle>> GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        [HttpGet("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Vehicle> GetVehicle([FromRoute] GetDeleteVehicleModel model)
        {
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(s => s.Id == model.Id);
            if (vehicle is not null)
            {
                return Ok(vehicle);
            }
            return NotFound();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Vehicle> PostVehicle(AddVehicleModel model)
        {
            if (parkingService.GetFreePlaces() > 0)
            {
                Vehicle vehicle = new Vehicle(model.Id, model.VehicleType, model.Balance);
                parkingService.AddVehicle(vehicle);
                return CreatedAtAction(nameof(GetVehicle), new { id = vehicle.Id },vehicle);
            }
            else
            {
                var errors = new { ParkingCapacity = new[] { "Parking is full" } };
                return BadRequest(new { Errors = errors, Title = "Bad Request", Status = 400 });
            }
        }

        [HttpDelete("{Id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteVehicle([FromRoute] GetDeleteVehicleModel model)
        {
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(s => s.Id == model.Id);
            if (vehicle is not null)
            {
                parkingService.RemoveVehicle(vehicle.Id);
                return NoContent();
            }
             return NotFound();
        }
    }
}
