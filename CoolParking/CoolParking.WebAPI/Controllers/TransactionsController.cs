﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public TransactionsController(IParkingService parking)
        {
            parkingService = parking;
        }

        [HttpGet("last")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return Ok(parkingService.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicleModel model)
        {
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(s => s.Id == model.Id);
            if(vehicle is null)
            {
                return NotFound();
            }
            parkingService.TopUpVehicle(model.Id, model.Sum);
            return Ok(vehicle);
        }
    }
}
