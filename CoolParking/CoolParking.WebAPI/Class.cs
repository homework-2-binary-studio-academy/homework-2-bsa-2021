﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI
{
    public class Class 
    {
        readonly ITimerService timerWithdrawService = new TimerService(Settings.InitPeriodDebtRelief * 1000);
        readonly ITimerService timerLogService = new TimerService(Settings.InitPeriodLogWrite * 1000);
        readonly ILogService logService = new LogService("Transactions.log");
        readonly IParkingService parkingService;

        public Class()
        {
            parkingService = new ParkingService(timerWithdrawService, timerLogService, logService);
            System.IO.File.Delete(logService.LogPath);
            timerLogService.Start();
            timerWithdrawService.Start();
            Vehicle vehicle = new Vehicle("AA-0000-SS", VehicleType.Bus, 1000);
            parkingService.AddVehicle(vehicle);
        }

        public ITimerService TimerWithdrawService => timerWithdrawService;

        public ITimerService TimerLogService => timerLogService;

        public ILogService LogService => logService;

        public IParkingService ParkingService => parkingService;
    }


}
