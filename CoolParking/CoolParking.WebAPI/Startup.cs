using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Validators;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Class>();
            services.AddTransient<ILogService, LogService>(_ => new LogService(Configuration["LogPath"]));
            services.AddSingleton<IParkingService, ParkingService>(provider => 
            {
                ITimerService timerWithdrawService = new TimerService(Settings.InitPeriodDebtRelief * 1000);
                ITimerService timerLogService = new TimerService(Settings.InitPeriodLogWrite * 1000);
                ParkingService parkingService = ActivatorUtilities.CreateInstance<ParkingService>(provider, timerWithdrawService, timerLogService);
                timerLogService.Start();
                timerWithdrawService.Start();
                return parkingService;
            });

            services.AddControllers().AddFluentValidation(); 
            services.AddTransient<IValidator<AddVehicleModel>, AddVehicleValidator>();
            services.AddTransient<IValidator<GetDeleteVehicleModel>, GetDeleteVehicleValidator>();
            services.AddTransient<IValidator<TopUpVehicleModel>, TopUpVehicleValidator>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoolParking.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoolParking.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
