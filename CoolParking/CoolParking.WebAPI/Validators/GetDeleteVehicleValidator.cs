﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Validators
{
    public class GetDeleteVehicleValidator : AbstractValidator<GetDeleteVehicleModel>
    {
        public GetDeleteVehicleValidator()
        {
            RuleFor(x => x.Id).Cascade(CascadeMode.Stop)
                .NotNull()
                .WithMessage("Id can't be null")
                .NotEmpty()
                .WithMessage("Id can't be empty")
                .Length(10)
                .WithMessage("Length must equel 10")
                .Must(x => Regex.IsMatch(x, Vehicle.Pattern))
                .WithMessage("Id do not response to pattern");
        }
    }
}
