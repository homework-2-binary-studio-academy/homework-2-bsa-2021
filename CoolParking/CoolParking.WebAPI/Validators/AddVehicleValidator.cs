﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Validators
{
    public class AddVehicleValidator : AbstractValidator<AddVehicleModel>
    {

        public AddVehicleValidator(IParkingService service)
        {
            RuleFor(x => x.Id).Cascade(CascadeMode.Stop)
                              .NotNull()
                              .WithMessage("id can't be null")
                              .NotEmpty()
                              .WithMessage("Id can't be empty")
                              .Length(10)
                              .WithMessage("Length must equel 10")
                              .Must(x => Regex.IsMatch(x, Vehicle.Pattern))
                              .WithMessage("Id do not response to pattern")
                              .Must(x => !service.GetVehicles().Any(y => y.Id == x))
                              .WithMessage("This vehicle Id is already exists");
            RuleFor(x => x.VehicleType).Must(x => Settings.InitTariffs.ContainsKey(x))
                                       .WithMessage("There is no such type");
            RuleFor(x => x.Balance).Must(x => x >= 0)
                                   .WithMessage("start balance can't be negative");

        }
    }
}
