﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking parking = Parking.GetParking();
        ITimerService withdrawTimerServ;
        ITimerService logTimerServ;
        ILogService logService1;
        List<TransactionInfo> transactionInfoList = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            withdrawTimerServ = withdrawTimer ?? throw new ArgumentNullException(nameof(withdrawTimer));
            logTimerServ = logTimer ?? throw new ArgumentNullException(nameof(logTimer));
            logService1 = logService ?? throw new ArgumentNullException(nameof(logService));
            withdrawTimerServ.Elapsed += WithdrawTimerServ_Elapsed;
            logTimerServ.Elapsed += LogTimerServ_Elapsed;
        }

        private void LogTimerServ_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(transactionInfoList.Count != 0)
            {
                foreach (var item in transactionInfoList)
                {
                    logService1.Write(item.ToString());
                }
                transactionInfoList.Clear();
            }
            else
            {
                logService1.Write("there is no transactions this time");
            }
        }

        private void WithdrawTimerServ_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vech in parking.Vehicles)
            {
                var debt = CulculateDebt(vech);
                vech.Balance -= debt;
                parking.Balance += debt;
                transactionInfoList.Add(new TransactionInfo(debt, vech.Id));
            }
        }

        private decimal CulculateDebt(Vehicle vech)
        {
            if (vech is null)
            {
                throw new ArgumentNullException(nameof(vech));
            }
            var type = vech.VehicleType;
            var pay = Settings.InitTariffs[type];
            if(vech.Balance < 0)
            {
                return pay * Settings.InitPenaltyRate;
            }
            if(vech.Balance < pay)
            {
                return vech.Balance + (pay - vech.Balance) * Settings.InitPenaltyRate;
            }
            return pay;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle is null)
            {
                throw new ArgumentNullException(nameof(vehicle));
            }
            if (parking.Vehicles.Count < parking.Capacity)
            {
                if (parking.Vehicles.Select(s=> s.Id).Contains(vehicle.Id))
                {
                    throw new ArgumentException("such Id already exists");
                }
                parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Dispose()
        {
            withdrawTimerServ.Dispose();
            logTimerServ.Dispose();
            parking.Vehicles.Clear();
            parking.Balance = Settings.InitBalanceParking;
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return parking.Capacity - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfoList.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return logService1.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (vehicleId is null)
            {
                throw new ArgumentNullException(nameof(vehicleId));
            }

            Vehicle vehicleItem;
            try
            {
                vehicleItem = parking.Vehicles.First(s => s.Id == vehicleId);
            }
            catch (InvalidOperationException ex) 
            {
                throw new ArgumentException("such vehicle is not exist", ex);
            }
            
            if(vehicleItem.Balance < 0)
            {
                throw new InvalidOperationException("balance is negative");
            }
            parking.Vehicles.Remove(vehicleItem);
            
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (vehicleId is null)
            {
                throw new ArgumentNullException(nameof(vehicleId));
            }
            if(sum < 0)
            {
                throw new ArgumentException("sum can`t be negative");
            }
            Vehicle vehicleItem;
            try
            {
                vehicleItem = parking.Vehicles.First(s => s.Id == vehicleId);
            }
            catch (InvalidOperationException ex)
            {
                throw new ArgumentException("such vehicle is not exist", ex);
            }
            vehicleItem.Balance += sum;
        }
        

    }
}
