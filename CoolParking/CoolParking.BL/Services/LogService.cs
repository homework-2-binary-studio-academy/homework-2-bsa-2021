﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException($"{nameof(path)} can`t be empty or null", nameof(path));
            }

            LogPath = path;
        }
        public string Read()
        {
            if (File.Exists(LogPath))
            {
                return File.ReadAllText(LogPath);
            }
            else
            {
                throw new InvalidOperationException("file is not found");
            }
            
        }

        public void Write(string logInfo)
        {
            if (File.Exists(LogPath))
            {
                using StreamWriter sw = File.AppendText(LogPath);
                sw.WriteLine(logInfo);
            }
            else
            {
                using StreamWriter sw = File.CreateText(LogPath);
                sw.WriteLine(logInfo);
            }
        }
    }
}