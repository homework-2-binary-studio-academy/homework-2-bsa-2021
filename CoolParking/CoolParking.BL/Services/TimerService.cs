﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        readonly Timer timer;
        private double interval;

        public double Interval { get => interval; set { interval = value; timer.Interval = value; } }

        public event ElapsedEventHandler Elapsed { add { timer.Elapsed += value; } remove { timer.Elapsed -= value; } }

        public TimerService(double interval)
        {
            this.interval = interval;
            timer = new Timer(interval);

        }
        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}