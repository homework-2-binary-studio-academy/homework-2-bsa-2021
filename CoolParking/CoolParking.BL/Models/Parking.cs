﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        public decimal Balance { get; set; }
        public int Capacity { get; set; }

        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();

        private Parking()
        {
            Balance = Settings.InitBalanceParking;
            Capacity = Settings.InitCapacityParking;
        }
        
        public static Parking GetParking()
        {
            if (parking is null)
            {
                parking = new Parking();
            }
            return parking;
        }

    }
}