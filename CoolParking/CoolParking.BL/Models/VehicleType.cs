﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        Bus,
        Motorcycle,
        PassengerCar,
        Truck,
    }
}
