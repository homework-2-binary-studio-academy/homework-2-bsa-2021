﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitBalanceParking { get; } 

        public static int InitCapacityParking { get; } 

        public static int InitPeriodDebtRelief { get; }

        public static int InitPeriodLogWrite { get; }

        public static Dictionary<VehicleType, decimal> InitTariffs { get; } = new Dictionary<VehicleType, decimal>();                      

        public static decimal InitPenaltyRate { get; }

        static Settings()
        {
            IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonFile("appSettings.json")
            .Build();
            InitBalanceParking = decimal.Parse(configuration["Settings:InitBalanceParking"], CultureInfo.InvariantCulture);
            InitCapacityParking = int.Parse(configuration["Settings:InitCapacityParking"]);
            InitPeriodDebtRelief = int.Parse(configuration["Settings:InitPeriodDebtRelief"]);
            InitPeriodLogWrite = int.Parse(configuration["Settings:InitPeriodLogWrite"]);
            string vehiclesDict = "Settings:InitTariffs:VehicleType.";
            var setcollection = configuration.AsEnumerable().Where(c => c.Key.StartsWith(vehiclesDict));
            foreach (var item in setcollection)
            {
                InitTariffs.Add((VehicleType)Enum.Parse(typeof(VehicleType), item.Key.Remove(0, vehiclesDict.Length)), decimal.Parse(item.Value, CultureInfo.InvariantCulture));
            }
            InitPenaltyRate = decimal.Parse(configuration["Settings:InitPenaltyRate"], CultureInfo.InvariantCulture);
        }
    }
}