﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using Fare;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; init; }
        public VehicleType VehicleType { get; init; }
        public decimal Balance { get; internal set; }

        public static string Pattern => "[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        public Vehicle(string uniqueId, VehicleType type, decimal balance)
        {
            if (Regex.IsMatch(uniqueId, Pattern))
            {
                this.Id = uniqueId;
            }
            else
            {
                throw new ArgumentException("id is not correspond to pattern", nameof(uniqueId));
            }

            if (Settings.InitTariffs.ContainsKey(type))
            {
                this.VehicleType = type;
            }
            else
            {
                throw new ArgumentException("there is no such vehicle type",nameof(type));
            }
            if (balance >= 0)
            {
                this.Balance = balance;
            }
            else
            {
                throw new ArgumentException(nameof(balance));
            }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var xeger = new Xeger(Pattern);
            string result = xeger.Generate();
            return result;
        }
    }
}