﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            TransactionDate = DateTime.Now;
            VehicleId = vehicleId;
        }

        public decimal Sum { get; }
        public DateTime TransactionDate { get; }
        public string VehicleId { get; }
        public override string ToString()
        {
            return $"[{TransactionDate}] Transport ID = {VehicleId} Sum = {Sum}";
        }
    }
}