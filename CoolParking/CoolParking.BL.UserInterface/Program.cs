﻿using System;

namespace CoolParking.BL.UserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            manager.Run();
        }
    }
}
