﻿using System;

namespace CoolParking.BL.UserInterface
{
    internal class Manager
    {
        bool IsExit;
        private int choice;

        public Manager()
        {
            IsExit = false;
        }

        public void Run()
        {
            using ParkingServicesModel servicesModel = new ParkingServicesModel();
            Console.WriteLine("Hi! Welcome to our Cool Parking!\n");

            while (!IsExit)
            {
                PrintMenu();
                do
                {
                    Console.WriteLine("\nEnter correct number\n");

                } while (!int.TryParse(Console.ReadLine(), out choice));

                switch (choice)
                {
                    case 1:
                        servicesModel.DisplayCurrentPakingBalance();
                        break;
                    case 2:
                        servicesModel.DisplayEarnedMoneyLastPeriod();
                        break;
                    case 3:
                        servicesModel.DisplayFreeAndOccupiedPlaces();
                        break;
                    case 4:
                        servicesModel.DisplayAllTransactionsByThePeriod();
                        break;
                    case 5:
                        servicesModel.PrintAllTransactions();
                        break;
                    case 6:
                        servicesModel.DisplayVehiclesInParking();
                        break;
                    case 7:
                        servicesModel.PutVehicleOnParking();
                        break;
                    case 8:
                        servicesModel.PickUpVehicle();
                        break;
                    case 9:
                        servicesModel.TopUpBalanceOfVehicle();
                        break;
                    default:
                        break;
                }
                IsExit = choice == 0; 
            }
        }



        private void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Please choose an opperation, ENTER its number and press Enter\n");
            Console.WriteLine("1) Display the current balance of the Parking - ENTER 1");
            Console.WriteLine("2) Display the amount of money earned for the current period (before logging) - ENTER 2");
            Console.WriteLine("3) Display the number of free / occupied parking spaces - ENTER 3");
            Console.WriteLine("4) Display all Parking Transactions for the current period (before logging) - ENTER 4");
            Console.WriteLine("5) Print the transaction history (by reading data from the Transactions.log file) - ENTER 5");
            Console.WriteLine("6) Display the list of Vehicles in the Parking lot - ENTER 6");
            Console.WriteLine("7) Put the Vehicle in the Parking Lot - ENTER 7");
            Console.WriteLine("8) Pick up a vehicle from the Parking lot - ENTER 8");
            Console.WriteLine("9) Top up the balance of a specific vehicle - ENTER 9");
            Console.WriteLine("0) To EXIT - ENTER 0");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}