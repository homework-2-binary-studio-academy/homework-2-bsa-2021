﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CoolParking.BL.UserInterface
{
    public class ParkingServicesModel : IDisposable
    {
        readonly TimerService timerWithdrawService = new TimerService(Settings.InitPeriodDebtRelief * 1000);
        readonly TimerService timerLogService = new TimerService(Settings.InitPeriodLogWrite * 1000);
        readonly LogService logService = new LogService("Transactions.log");
        readonly ParkingService parkingService;

        public ParkingServicesModel()
        {
            parkingService = new ParkingService(timerWithdrawService, timerLogService, logService);
            File.Delete(logService.LogPath);
            timerLogService.Start();
            timerWithdrawService.Start();
        }

        public void DisplayCurrentPakingBalance()
        {
            Console.WriteLine($"Current balance of Parking = {parkingService.GetBalance()}\n");
        }

        public void DisplayEarnedMoneyLastPeriod()
        {
            TransactionInfo[] arr = parkingService.GetLastParkingTransactions();
            Console.WriteLine($"Earned money in this period : {arr.Select(s => s.Sum).Sum()}\n");
        }

        public void DisplayFreeAndOccupiedPlaces()
        {
            int freePlaces = parkingService.GetFreePlaces();
            Console.WriteLine($"Free places : {freePlaces}");
            Console.WriteLine($"Occupied places : {parkingService.GetCapacity() - freePlaces}\n");
        }
        public void DisplayAllTransactionsByThePeriod()
        {
            TransactionInfo[] arr = parkingService.GetLastParkingTransactions();
            if (arr.Length == 0)
            {
                Console.WriteLine("There is no transactions\n");
            }
            else
            {
                foreach (var item in arr)
                {
                    Console.WriteLine($"[{item.TransactionDate}]  {item.Sum} was debited from {item.VehicleId}\n");
                }
            }

        }

        public void PrintAllTransactions()
        {
            try
            {
                Console.WriteLine(logService.Read());
            }
            catch (InvalidOperationException ex)
            {
                SomethingWentWrong();

                Console.WriteLine(ex.Message);
            }

        }

        public void DisplayVehiclesInParking()
        {
            ReadOnlyCollection<Vehicle> vehArr = parkingService.GetVehicles();
            if (vehArr.Count == 0)
            {
                Console.WriteLine("There is no vehicles in Parking\n");
            }
            else
            {
                foreach (var item in vehArr)
                {
                    Console.WriteLine($"Vehicle ID : {item.Id}\t\t Type : {item.VehicleType}\t\t Balance : {item.Balance}\n");
                }
            }

        }

        public void TopUpBalanceOfVehicle()
        {
            Console.WriteLine("Enter a vehicle ID : (Format: XX-YYYY-XX)");
            string vehId = Console.ReadLine();
            decimal sum;
            do
            {
                Console.WriteLine("Enter sum of refillng");

            } while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out sum));


            try
            {
                parkingService.TopUpVehicle(vehId, sum);
                Console.WriteLine($"Super! Your vehicle balance now : {parkingService.GetVehicles().First(s => s.Id == vehId).Balance}\n");
            }
            catch (ArgumentException ex)
            {
                SomethingWentWrong();
                Console.WriteLine(ex.Message);
            }

        }

        private static void SomethingWentWrong()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Something went wrong! Try again");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        public void PutVehicleOnParking()
        {
            Console.WriteLine("Enter your vehicle ID : (Format: XX-YYYY-XX) or enter to generate it automaticaly");
            string vehId = Console.ReadLine();
            if (vehId == String.Empty)
            {
                vehId = Vehicle.GenerateRandomRegistrationPlateNumber();
            }
            object type;
            do
            {
                Console.WriteLine("Enter you vehicle type (Bus, Motorcycle, PassengerCar, Truck)\n");

            } while (!Enum.TryParse(typeof(VehicleType), Console.ReadLine(), out type));

            decimal bal;
            do
            {
                Console.WriteLine("Enter start balance on a vehicle");

            } while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out bal));

            Vehicle newVehicle;
            try
            {
                newVehicle = new Vehicle(vehId, (VehicleType)type, bal);
                parkingService.AddVehicle(newVehicle);
                Console.WriteLine("\nThe vehicle was successfully added\n");
            }
            catch (ArgumentException ex)
            {
                SomethingWentWrong();
                Console.WriteLine(ex.Message);
            }


        }
        public void PickUpVehicle()
        {
            Console.WriteLine("Enter your vehicle ID : (Format: XX-YYYY-XX)");
            string vehId = Console.ReadLine();
            try
            {
                parkingService.RemoveVehicle(vehId);
            }
            catch (ArgumentException ex)
            {
                SomethingWentWrong();

                Console.WriteLine(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                SomethingWentWrong();

                Console.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
            parkingService.Dispose();
        }
    }
}
