﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.BL.UserInterface
{
    internal class Manager : IDisposable
    {
        private bool isExit;
        private int choice;
        private readonly HttpClient client;

        public Manager()
        {
            isExit = false;
            client = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:44306/api/")
            };
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task RunAsync()
        {
            ParkingServicesModel servicesModel = new ParkingServicesModel(client);
            Console.WriteLine("Hi! Welcome to our Cool Parking!\n");

            while (!isExit)
            {
                PrintMenu();
                do
                {
                    Console.WriteLine("\nEnter correct number\n");

                } while (!int.TryParse(Console.ReadLine(), out choice));

                switch (choice)
                {
                    case 1:
                        await servicesModel.DisplayCurrentPakingBalanceAsync();
                        break;
                    case 2:
                        await servicesModel.DisplayEarnedMoneyLastPeriodAsync();
                        break;
                    case 3:
                        await servicesModel.DisplayFreeAndOccupiedPlacesAsync();
                        break;
                    case 4:
                        await servicesModel.DisplayAllTransactionsByThePeriodAsync();
                        break;
                    case 5:
                        await servicesModel.DisplayAllTransactionsAsync();
                        break;
                    case 6:
                        await servicesModel.DisplayVehiclesInParkingAsync();
                        break;
                    case 7:
                        await servicesModel.PutVehicleOnParkingAsync();
                        break;
                    case 8:
                        await servicesModel.PickUpVehicleAsync();
                        break;
                    case 9:
                        await servicesModel.TopUpBalanceOfVehicleAsync();
                        break;
                    default:
                        break;
                }
                isExit = choice == 0;
            }
        }



        private void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Please choose an opperation, ENTER its number and press Enter\n");
            Console.WriteLine("1) Display the current balance of the Parking - ENTER 1");
            Console.WriteLine("2) Display the amount of money earned for the current period (before logging) - ENTER 2");
            Console.WriteLine("3) Display the number of free / occupied parking spaces - ENTER 3");
            Console.WriteLine("4) Display all Parking Transactions for the current period (before logging) - ENTER 4");
            Console.WriteLine("5) Print the transaction history (by reading data from the Transactions.log file) - ENTER 5");
            Console.WriteLine("6) Display the list of Vehicles in the Parking lot - ENTER 6");
            Console.WriteLine("7) Put the Vehicle in the Parking Lot - ENTER 7");
            Console.WriteLine("8) Pick up a vehicle from the Parking lot - ENTER 8");
            Console.WriteLine("9) Top up the balance of a specific vehicle - ENTER 9");
            Console.WriteLine("0) To EXIT - ENTER 0");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}