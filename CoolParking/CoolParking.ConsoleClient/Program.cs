﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.BL.UserInterface
{
    class Program
    {
        static async Task Main(string[] args)
        {
            
            using Manager manager = new Manager();
            await manager.RunAsync();
            
        }
    }
}
