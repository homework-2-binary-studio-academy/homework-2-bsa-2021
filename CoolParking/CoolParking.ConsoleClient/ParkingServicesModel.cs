﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.ConsoleClient.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.BL.UserInterface
{
    public class ParkingServicesModel
    {
        public HttpClient Client { get; }

        public ParkingServicesModel(HttpClient client)
        {
            Client = client;
        }

        public async Task DisplayCurrentPakingBalanceAsync()
        {
            string path = "parking/balance";
            var response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Current balance of Parking = {await response.Content.ReadAsStringAsync()}\n");
            }
        }

        public async Task DisplayEarnedMoneyLastPeriodAsync()
        {
            string path = "Transactions/last";
            var response = await Client.GetAsync(path);
            TransactionInfoModel[] array;
            if (response.IsSuccessStatusCode)
            {
                array = JsonConvert.DeserializeObject<TransactionInfoModel[]>(await response.Content.ReadAsStringAsync());
                Console.WriteLine($"Earned money in this period : {array.Select(s => s.Sum).Sum()}\n");
            }
            else
            {
                await ErrorMessage(response);
            }
        }

        private static async Task ErrorMessage(HttpResponseMessage response)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Something terrible has happened");
            Console.WriteLine(response.Headers);
            Console.WriteLine(response.StatusCode);
            dynamic parsedJson = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
            var content = JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
            Console.WriteLine(content);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public async Task DisplayFreeAndOccupiedPlacesAsync()
        {
            string path = "parking/freePlaces";
            string pathToGetCapacity = "parking/capacity";
            var response = await Client.GetAsync(path);
            int freePlaces = 0;
            if (response.IsSuccessStatusCode)
            {
                freePlaces = JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());
                Console.WriteLine($"Free places : {freePlaces}");
            }
            else
            {
                await ErrorMessage(response);
            }
            var responseParkingCapacity = await Client.GetAsync(pathToGetCapacity);
            int capacity = JsonConvert.DeserializeObject<int>(await responseParkingCapacity.Content.ReadAsStringAsync());
            if (responseParkingCapacity.IsSuccessStatusCode)
            {
                Console.WriteLine($"Capacity { capacity }");
                Console.WriteLine($"Occupied places : { capacity - freePlaces}\n");
            }
            else
            {
                await ErrorMessage(responseParkingCapacity);
            }


        }
        public async Task DisplayAllTransactionsByThePeriodAsync()
        {
            string path = "Transactions/last";
            var response = await Client.GetAsync(path);
            TransactionInfoModel[] array;
            if (response.IsSuccessStatusCode)
            {
                array = JsonConvert.DeserializeObject<TransactionInfoModel[]>(await response.Content.ReadAsStringAsync());
                if (array.Length == 0)
                {
                    Console.WriteLine("There is no transactions this period.");
                }
                foreach (var item in array)
                {
                    Console.WriteLine($"[{item.TransactionDate}]\tSum = {item.Sum} was debited from {item.VehicleId}");
                }
            }
            else
            {
                await ErrorMessage(response);
            }
        }

        public async Task DisplayAllTransactionsAsync()
        {
            string path = "transactions/all";
            var response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
            else
            {
                await ErrorMessage(response);
            }
        }

        public async Task DisplayVehiclesInParkingAsync()
        {
            string path = "Vehicles";
            var response = await Client.GetAsync(path);
            ICollection<VehicleModel> array;
            if (response.IsSuccessStatusCode)
            {
                array = JsonConvert.DeserializeObject<ICollection<VehicleModel>>(await response.Content.ReadAsStringAsync());
                if (array.Count == 0)
                {
                    Console.WriteLine("There is no vehicles at the parking");
                }
                foreach (var item in array)
                {
                    Console.WriteLine($"Vehicle Id : {item.Id}\t Type : {item.VehicleType}\t Balance : {item.Balance}");
                }
            }
            else
            {
                await ErrorMessage(response);
            }
        }

        public async Task TopUpBalanceOfVehicleAsync()
        {
            string path = "transactions/topUpVehicle";
            Console.WriteLine("Enter a vehicle ID : (Format: XX-YYYY-XX)");
            string vehId = Console.ReadLine();
            decimal sum;
            do
            {
                Console.WriteLine("Enter sum of refillng");

            } while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out sum));

            string json = JsonConvert.SerializeObject(new { Id = vehId, Sum = sum });
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await Client.PutAsync(path, httpContent);
            if (response.IsSuccessStatusCode)
            {
                var result = JsonConvert.DeserializeObject<VehicleModel>(await response.Content.ReadAsStringAsync());
                Console.WriteLine($"Super! Your vehicle balance now : {result.Balance}\n"); 
            }
            else
            {
                await ErrorMessage(response);
            }

        }


        public async Task PutVehicleOnParkingAsync()
        {
            string path = "vehicles";
            Console.WriteLine("Enter your vehicle ID : (Format: XX-YYYY-XX) or enter to generate it automaticaly");
            string vehId = Console.ReadLine();
            if (vehId == String.Empty)
            {
                vehId = Vehicle.GenerateRandomRegistrationPlateNumber();
            }
            object type;
            do
            {
                Console.WriteLine("Enter you vehicle type (Bus, Motorcycle, PassengerCar, Truck)\n");

            } while (!Enum.TryParse(typeof(VehicleType), Console.ReadLine(), out type));

            decimal bal;
            do
            {
                Console.WriteLine("Enter start balance on a vehicle");

            } while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out bal));


            string json = JsonConvert.SerializeObject(new VehicleModel(vehId, (VehicleType)type, bal));
            StringContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await Client.PostAsync(path, httpContent);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("\nThe vehicle was successfully added\n");
            }
            else
            {
                await ErrorMessage(response);
            }

        }

        public async Task PickUpVehicleAsync()
        {

            Console.WriteLine("Enter your vehicle ID : (Format: XX-YYYY-XX)");
            string vehId = Console.ReadLine();
            string path = $"vehicles/{vehId}";
            var response = await Client.DeleteAsync(path);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Vehicle successfully deleted.");
            }
            else
            {
                await ErrorMessage(response);
            }
        }
    }
}
