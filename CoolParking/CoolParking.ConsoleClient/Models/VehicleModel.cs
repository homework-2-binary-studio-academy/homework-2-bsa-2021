﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleClient.Models
{
    class VehicleModel
    {
        public VehicleModel(string vehId, VehicleType type, decimal bal)
        {
            Id = vehId;
            VehicleType = type;
            Balance = bal;
        }

        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
  
    }
}
