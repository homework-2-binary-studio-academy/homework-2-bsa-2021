﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleClient.Models
{
    public class TransactionInfoModel
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
